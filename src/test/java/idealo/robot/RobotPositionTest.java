package idealo.robot;

import org.junit.Test;

import static idealo.robot.RobotDirection.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNull;

public class RobotPositionTest {
    RobotPosition middle = RobotPosition.create(2, 2);

    @Test
    public void createInvalid() {
        assertNull(RobotPosition.create(-2, 2));
        assertNull(RobotPosition.create(2, 5));
    }

    @Test
    public void moveEast() {
        assertThat(middle.move(EAST)).isEqualTo(RobotPosition.create(3, 2));
    }

    @Test
    public void moveWest() {
        assertThat(middle.move(WEST)).isEqualTo(RobotPosition.create(1, 2));
    }

    @Test
    public void moveNorth() {
        assertThat(middle.move(NORTH)).isEqualTo(RobotPosition.create(2, 3));
    }

    @Test
    public void moveSouth() {
        assertThat(middle.move(SOUTH)).isEqualTo(RobotPosition.create(2, 1));
    }

    @Test
    public void dontMoveEast() {
        assertThat(RobotPosition.create(4, 2).move(EAST))
                .isEqualTo(RobotPosition.create(4, 2));
    }

    @Test
    public void dontMoveWest() {
        assertThat(RobotPosition.create(0, 2).move(WEST))
                .isEqualTo(RobotPosition.create(0, 2));
    }

    @Test
    public void dontMoveSouth() {
        assertThat(RobotPosition.create(2, 0).move(SOUTH))
                .isEqualTo(RobotPosition.create(2, 0));
    }

    @Test
    public void dontMoveNorth() {
        assertThat(RobotPosition.create(2, 4).move(NORTH))
                .isEqualTo(RobotPosition.create(2, 4));
    }
}