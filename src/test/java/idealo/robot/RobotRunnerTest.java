package idealo.robot;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class RobotRunnerTest {
    RobotRunner runner = new RobotRunner();

    String shortResult(String input) {
        List<RobotTrace> traces = runner.run(input);
        return runner.reportsAsString(traces);
    }

    @Test
    public void simpleTest() {
        String input = "PLACE 0,0,NORTH\n" +
                "MOVE\n" +
                "RIGHT\n" +
                "MOVE\n" +
                "MOVE\n" +
                "REPORT\n";

        List<RobotTrace> traces = runner.run(input);
        assertThat(runner.reportsAsString(traces)).isEqualTo("2,1,EAST");
        assertThat(runner.tracesAsString(traces)).isEqualTo(
                "PLACE 0,0,NORTH  -  0,0,NORTH\n" +
                "MOVE             -  0,1,NORTH\n" +
                "RIGHT            -  0,1,EAST\n" +
                "MOVE             -  1,1,EAST\n" +
                "MOVE             -  2,1,EAST\n" +
                "REPORT           -  2,1,EAST");
    }

    @Test
    public void officialExamples() {
        assertThat(shortResult(
                "PLACE 0,0,NORTH \n" +
                        "MOVE \n" +
                        "REPORT ")
        ).isEqualTo("0,1,NORTH");

        assertThat(shortResult(
                "PLACE 0,0,NORTH\n" +
                        "LEFT\n" +
                        "REPORT")
        ).isEqualTo("0,0,WEST");

        assertThat(shortResult(
                "PLACE 1,2,EAST \n" +
                        "MOVE \n" +
                        "MOVE \n" +
                        "LEFT\n" +
                        "MOVE\n" +
                        "REPORT")
        ).isEqualTo("3,3,NORTH");

        assertThat(shortResult(
                "MOVE\n" +
                        "REPORT")
        ).isEqualTo("ROBOT MISSING");
    }

    @Test
    public void longTest() {
        String input = "MOVE\n" +
                "RIGHT\n" +
                "MOVE\n" +
                "PLACE 0,0,NORTH\n" +
                "MOVE\n" +
                "RIGHT\n" +
                "MOVE\n" +
                "REPORT\n" +
                "LEFT\n" +
                "MOVE\n" +
                "MOVE\n" +
                "REPORT\n" +
                "PLACE 5,0,NORTH\n" +
                "REPORT\n" +
                "PLACE 0,0,CENTER\n" +
                "REPORT\n" +
                "PLACE 4,4,WEST\n" +
                "MOVE\n" +
                "MOVE\n" +
                "RIGHT\n" +
                "MOVE\n" +
                "MOVE\n" +
                "RIGHT\n" +
                "MOVE\n" +
                "MOVE\n" +
                "REPORT\n";

        List<RobotTrace> traces = runner.run(input);
        assertThat(runner.reportsAsString(traces)).isEqualTo(
                "1,1,EAST\n" +
                "1,3,NORTH\n" +
                "1,3,NORTH\n" +
                "1,3,NORTH\n" +
                "4,4,EAST");
        assertThat(runner.tracesAsString(traces)).isEqualTo(
                "MOVE             -  ROBOT MISSING\n" +
                "RIGHT            -  ROBOT MISSING\n" +
                "MOVE             -  ROBOT MISSING\n" +
                "PLACE 0,0,NORTH  -  0,0,NORTH\n" +
                "MOVE             -  0,1,NORTH\n" +
                "RIGHT            -  0,1,EAST\n" +
                "MOVE             -  1,1,EAST\n" +
                "REPORT           -  1,1,EAST\n" +
                "LEFT             -  1,1,NORTH\n" +
                "MOVE             -  1,2,NORTH\n" +
                "MOVE             -  1,3,NORTH\n" +
                "REPORT           -  1,3,NORTH\n" +
                "PLACE 5,0,NORTH  -  1,3,NORTH\n" +
                "REPORT           -  1,3,NORTH\n" +
                "PLACE 0,0,CENTER  -  1,3,NORTH\n" +
                "REPORT           -  1,3,NORTH\n" +
                "PLACE 4,4,WEST   -  4,4,WEST\n" +
                "MOVE             -  3,4,WEST\n" +
                "MOVE             -  2,4,WEST\n" +
                "RIGHT            -  2,4,NORTH\n" +
                "MOVE             -  2,4,NORTH\n" +
                "MOVE             -  2,4,NORTH\n" +
                "RIGHT            -  2,4,EAST\n" +
                "MOVE             -  3,4,EAST\n" +
                "MOVE             -  4,4,EAST\n" +
                "REPORT           -  4,4,EAST");
    }
}