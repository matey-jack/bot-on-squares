package idealo.robot;

import org.junit.Test;

import static idealo.robot.RobotDirection.*;
import static org.assertj.core.api.Assertions.assertThat;

public class RobotDirectionTest {

    @Test
    public void leftCircle() {
        assertThat(SOUTH.left()).isEqualTo(EAST);
        assertThat(EAST.left()).isEqualTo(NORTH);
        assertThat(NORTH.left()).isEqualTo(WEST);
        assertThat(WEST.left()).isEqualTo(SOUTH);
    }

    @Test
    public void rightCircle() {
        assertThat(SOUTH.right()).isEqualTo(WEST);
        assertThat(WEST.right()).isEqualTo(NORTH);
        assertThat(NORTH.right()).isEqualTo(EAST);
        assertThat(EAST.right()).isEqualTo(SOUTH);
    }
}