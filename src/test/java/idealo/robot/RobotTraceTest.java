package idealo.robot;

import org.junit.Test;


import static idealo.robot.RobotDirection.EAST;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RobotTraceTest {

    @Test
    public void testConstructors() {
        assertTrue(new RobotTrace("move", EAST, RobotPosition.create(0, 0)).isPlaced());
        assertFalse(RobotTrace.unplaced("move").isPlaced());
    }
}