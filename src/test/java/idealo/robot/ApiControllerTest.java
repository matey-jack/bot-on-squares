package idealo.robot;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ApiControllerTest {
    // RobotRunner is so simple that we don't need to mock it, but this would be the place to easily use a mock instead.
    ApiController controller = new ApiController(new RobotRunner());

    @Test
    public void testDebug() {
        // we use very simple commands here so that this test will not need changing when RobotRunner is extended.
        // this strategy often makes the tests more robust than mocks would, since the interface between Controller
        // and Service is not specified here. (That's what RobotRunnerTest is for.)
        assertThat(controller.simulate("REPORT", false)).isEqualTo("ROBOT MISSING");
        assertThat(controller.simulate("REPORT", true)).isEqualTo("REPORT           -  ROBOT MISSING");
    }
}