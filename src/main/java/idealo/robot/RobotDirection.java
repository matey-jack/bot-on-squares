package idealo.robot;

import lombok.AllArgsConstructor;

@AllArgsConstructor
enum RobotDirection {
    // use clock-wise order, so turning right means next direction in this order, left means previous (modulo 4).
    NORTH(0, 1), EAST(1, 0), SOUTH(0, -1), WEST(-1, 0);

    final int dx, dy;

    public RobotDirection left() {
        return RobotDirection.values()[(this.ordinal() + 3) % 4];
    }

    public RobotDirection right() {
        return RobotDirection.values()[(this.ordinal() + 1) % 4];
    }
}
