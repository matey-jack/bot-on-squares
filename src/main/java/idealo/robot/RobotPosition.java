package idealo.robot;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@EqualsAndHashCode
public class RobotPosition {
    public final int x, y;

    public static RobotPosition create(int x, int y) {
        if (inBounds(x, xSize) && inBounds(y, ySize)) {
            return new RobotPosition(x, y);
        }
        return null;
    }

    static boolean inBounds(int value, int size) {
        return value >= 0 && value < size;
    }

    final static int xSize = 5;
    final static int ySize = 5;

    public RobotPosition move(RobotDirection direction) {
        int nx = x + direction.dx;
        int ny = y + direction.dy;
        if (inBounds(nx, xSize) && inBounds(ny, ySize)) {
            return new RobotPosition(nx, ny);
        }
        return this;
    }
}
