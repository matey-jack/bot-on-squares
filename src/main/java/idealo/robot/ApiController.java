package idealo.robot;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequiredArgsConstructor
public class ApiController {
    private final RobotRunner robotRunner;

    @RequestMapping(value = "/simulate", method = POST, produces = "text/plain")
    @ResponseBody
    public String simulate(@RequestBody String input, @RequestParam(required = false) boolean debug) {
        List<RobotTrace> traces = robotRunner.run(input);
        if (debug) {
            return robotRunner.tracesAsString(traces);
        }
        return robotRunner.reportsAsString(traces);
    }


    @RequestMapping(value = "/form/result", method = POST, produces = "text/html")
    public ModelAndView formResult(@RequestParam String commands) {
        // note to self: prefer ModelAndView to Model + (String viewname) because we can also set status code,
        // do redirections etc. pp.
        List<RobotTrace> traces = robotRunner.run(commands);
        return new ModelAndView("result")
                .addObject("traces", traces);
    }

}
