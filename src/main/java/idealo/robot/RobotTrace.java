package idealo.robot;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class RobotTrace {
    public final String command;
    public final RobotDirection direction;
    public final RobotPosition position;

    public RobotTrace(String command, RobotDirection direction, RobotPosition position) {
        this.command = command.toUpperCase();
        this.direction = direction;
        this.position = position;
    }

    public static RobotTrace unplaced(String command) {
        return new RobotTrace(command, null, null);
    }

    public static RobotTrace noOp(String command, RobotTrace lastState) {
        return (lastState == null)
                ? RobotTrace.unplaced(command)
                : new RobotTrace(command, lastState.direction, lastState.position);
    }

    public boolean isPlaced() {
        return direction != null && position != null;
    }

    public String stateString() {
        return isPlaced()
                ? String.format("%d,%d,%s", position.x, position.y, direction)
                : "ROBOT MISSING";
    }

    public String fullString() {
        // pad using length of longest command:"place x,y,north"
        return String.format("%-15s  -  %s", command, stateString());
    }

    public boolean isReport() {
        return "report".equals(command.trim().toLowerCase());
    }
}
