package idealo.robot;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.lang.Integer.parseInt;
import static java.util.stream.Collectors.joining;

@Service
@RequiredArgsConstructor
public class RobotRunner {
    public List<RobotTrace> run(String input) {
        ArrayList<RobotTrace> result = new ArrayList<>();
        RobotTrace lastState = null;
        for (String line : input.split("\n")) {
            lastState = nextState(line.toLowerCase().trim(), lastState);
            result.add(lastState);
        }
        return result;
    }

    private RobotTrace nextState(String line, RobotTrace lastState) {
        String[] tokens = line.split(" +");
        if (tokens.length < 1) return lastState;

        if ("place".equals(tokens[0])) {
            return place(tokens).orElse(RobotTrace.noOp(line, lastState));
        }
        if (lastState == null || !lastState.isPlaced()) return RobotTrace.noOp(line, lastState);

        switch (tokens[0]) {
            case "move":
                return new RobotTrace(line, lastState.direction, lastState.position.move(lastState.direction));
            case "left":
                return new RobotTrace(line, lastState.direction.left(), lastState.position);
            case "right":
                return new RobotTrace(line, lastState.direction.right(), lastState.position);
        }
        return RobotTrace.noOp(line, lastState);
    }

    private Optional<RobotTrace> place(String[] tokens) {
        String[] params = tokens[1].split(",");
        RobotTrace result;
        try {
            result = new RobotTrace(
                    String.join(" ", tokens),
                    RobotDirection.valueOf(params[2].toUpperCase()),
                    RobotPosition.create(parseInt(params[0]), parseInt(params[1])));
            if (result.isPlaced()) return Optional.of(result);
        } catch (IllegalArgumentException e) {
            // fallthrough is just right
        }
        return Optional.empty();
    }

    public String tracesAsString(List<RobotTrace> traces) {
        return traces.stream()
                .map(RobotTrace::fullString)
                .collect(joining("\n"));
    }

    public String reportsAsString(List<RobotTrace> traces) {
        return traces.stream()
                .filter(RobotTrace::isReport)
                .map(RobotTrace::stateString)
                .collect(joining("\n"));
    }
}
