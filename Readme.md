
Run and try it out
==================

    mvn spring-boot:run
    
    curl -H "Content-Type: text/plain" --data-binary @src/test/resources/example_input.txt http://localhost:8080/simulate
    
or the verbose mode:

    curl -H "Content-Type: text/plain" --data-binary @src/test/resources/example_input.txt http://localhost:8080/simulate?debug=1

Automatic test cases all run as unit tests without a started server. Start as usual:

    mvn test

In a real project, I'd add at least one automatic integration test (some preparation is already in `pom.xml`) to check
that Spring actually starts the server.


Interactive version
===================

Good design always makes it easy to add more views and so I added 
[a Browser version](http://localhost:8080/form/index.html), too.

 

Specification of special cases
==============================

  - PLACE command with coordinates outside playing field will be ignored

  - all PLACE commands will be executed. Consequentially, all commands preceding the last valid 
    PLACE command will be inconsequential for the final result.
  
  - unrecognized commands will be ignored
  
  - as a convenience, case is ignored when reading input
  
  - there's one line of output for every REPORT command

Design choices
==============

All data classes are immutable and all intermediate state of RobotRunner is in local variables. So the program should 
naturally be thread-safe, although I didn't run a stress-test to check.

Note that immutability comes very naturally with the `enum` class and is needed for the trace class, and then I also 
adopted it for the Position to have everything in the same style.

I used Spring DI via constructors which makes it very easy to manually instantiate classes in tests. 